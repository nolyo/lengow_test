from django.conf.urls import url

from . import views

app_name = 'orders'
urlpatterns = [
    url(r'^$', views.listOrders, name='list'),
    url(r'^lunch', views.lunch, name='lunch'),
    url(r'^trun', views.truncate, name='truncate'),
    url(r'^search', views.search, name='search'),
    url(r'^(?P<pk>[0-9]+)/$', views.DetailView.as_view(), name='detail'),
]

