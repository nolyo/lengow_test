import xml.dom.minidom

try:
    from urllib.request import urlopen
except ImportError:
    from urllib2 import urlopen
    
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.utils import timezone
from django.views import generic

from orders.models import Orders, Products, MarketPlace


def lunch(request):
    orders_xml = urlopen('http://test.lengow.io/orders-test.xml')
    DOMTree = xml.dom.minidom.parse(orders_xml)
    collection = DOMTree.documentElement
    # Get all Orders
    orders = collection.getElementsByTagName("order")
    orderByOrder(orders)
    return HttpResponseRedirect(reverse('orders:list'))


def orderByOrder(orders):
    for order in orders:

        marketplace = getMarketplace(order)
        order_id = getData(order, 'order_id')
        payment_date = getData(order, 'payment_date')
        if payment_date == False:
            payment_date = timezone.now()
        order_amount = getData(order, 'order_amount')
        order_currency = getData(order, 'order_currency')

        save_order = Orders(order_id=order_id, payment_date=payment_date,
                            order_amount=order_amount, order_currency=order_currency, marketplace=marketplace)
        save_order.save()

        # # Get all products in order
        products = order.getElementsByTagName("product")
        for product in products:
            title = getData(product, 'title')
            url_image = getData(product, 'url_image')
            price_unit = getData(product, 'price_unit')
            quantity = getData(product, 'quantity')

            save_product = Products(
                title=title,
                url_image=url_image,
                price_unit=price_unit,
                quantity=quantity,
                order=save_order
            )

            save_product.save()


def search(request):
    all_marketplace = MarketPlace.objects.all()
    marketplace = request.GET.get('marketplace')
    date = request.GET.get('date')
    devise = request.GET.get('devise')
    amount = request.GET.get('amount')
    if marketplace == "all":
        all_orders = Orders.objects.all()
    else:
        all_orders = Orders.objects.filter(marketplace__name=marketplace)

    if date:
        all_orders = all_orders.filter(payment_date=date)
    if devise:
        all_orders = all_orders.filter(order_currency=devise)
    if amount:
        all_orders = all_orders.filter(order_amount__gte=amount)
    return render(request, 'orders/search.html', {'all_orders': all_orders, 'all_marketplace': all_marketplace})


def getMarketplace(order):
    marketplace = order.getElementsByTagName("marketplace")[0]
    marketplace_data = marketplace.childNodes[0].data
    marketplace = MarketPlace.objects.filter(name=marketplace_data)
    if not marketplace:
        m = MarketPlace(name=marketplace_data)
        m.save()
        return m
    else:
        return marketplace[0]


def getData(order, tag_name):
    a = order.getElementsByTagName(tag_name)[0]
    if a.firstChild:
        return a.firstChild.data
    else:
        return False


def listOrders(request):
    all_orders = Orders.objects.order_by('-payment_date')[:25]
    all_marketplace = MarketPlace.objects.all()

    return render(request, 'orders/list_orders.html', {'all_orders': all_orders, 'all_marketplace': all_marketplace})


class DetailView(generic.DetailView):
    model = Orders
    template_name = 'orders/detail.html'


def truncate(request):
    Orders.objects.all().delete()
    Products.objects.all().delete()
    MarketPlace.objects.all().delete()

    return HttpResponseRedirect(reverse('orders:list'))
