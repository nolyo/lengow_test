from __future__ import unicode_literals

from datetime import datetime

from django.db import models


class MarketPlace(models.Model):
    name = models.CharField(max_length=200)
    def __str__(self):
        return self.name


class Orders(models.Model):
    marketplace = models.ForeignKey(MarketPlace, on_delete=models.CASCADE)
    payment_date = models.DateField(default=datetime.now, blank=True)
    order_amount = models.FloatField(default=0)
    order_id = models.CharField(max_length=255)
    order_currency = models.CharField(max_length=20)

    def __str__(self):
        return self.order_id


class Products(models.Model):
    title = models.CharField(max_length=255)
    url_image = models.TextField()
    price_unit = models.FloatField(default=0)
    quantity = models.IntegerField(default=0)
    order = models.ForeignKey(Orders, on_delete=models.CASCADE)

    def __str__(self):
        return self.title
